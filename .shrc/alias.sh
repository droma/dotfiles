# Regular comands
alias c="clear"
alias wget="wget -c"
alias v="vim"
alias m="mutt"
alias mutt="neomutt"
alias e="exit"
#alias tmux="TERM=xterm-256color tmux"
alias mat="matlab -nodesktop -nosplash"
alias ls="ls --color=always"
alias z="ls -lha"
alias open="xdg-open"
alias o="open"
alias p="mpv"
alias mp="cmus"
alias fetch="neofetch --ascii_distro arch_small --disable cpu memory resolution theme icons gpu packages uptime kernel term_font model"
alias xclip="xclip -selection c"

# Prefer Neovim.
alias vim="nvim"

# transmission remote
alias t="transmission-remote -l"
alias ts="transmission-remote-server -l"

# git
alias g="git"
alias gs="git status"
alias ga="git add --all"
alias gc="git commit -m"
alias gl="git log --oneline"

# For distros whose shutdown/reboot require super user privileges (eg. Void)
alias shutdown="sudo shutdown"
alias reboot="sudo reboot"
