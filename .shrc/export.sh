export BROWSER=/usr/bin/qutebrowser
export VISUAL="nvim"
export EDITOR="nvim"
export RANGER_LOAD_DEFAULT_RC="FALSE"

# For torrench to work with transmission-remote
export TR_AUTH=":"

# RTV: Reddit Terminal View
export RTV_EDITOR="nvim"
export RTV_BROWSER=/usr/bin/qutebrowser

# Matlab
#export MATLAB_JAVA=/usr/lib/jvm/java-7-openjdk/jre
export MATLAB_JAVA=/usr/local/MATLAB/R2017a/sys/java/jre/glnxa64/jre

# Docker.
export DOCKER_HOST=unix:///var/run/docker.sock

# CMake defaults.
export CMAKE_GENERATOR="Ninja Multi-Config"

# C/C++ compilers.
# force C colored diagnostic output.
export CFLAGS="${CFLAGS} -fdiagnostics-color=always"
# force C++ colored diagnostic output.
export CXXFLAGS="${CXXFLAGS} -fdiagnostics-color=always"
export CCFLAGS="${CCFLAGS} -fdiagnostics-color=always"
# force C, C++, Cpp (pre-processor) colored diagnostic output.
export CPPFLAGS="${CPPFLAGS} -fdiagnostics-color=always"
