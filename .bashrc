#
# ~/.bashrc
#
source $HOME/.shrc/path.sh

[[ $- != *i* ]] && return

colors() {
	local fgc bgc vals seq0

	printf "Color escapes are %s\n" '\e[${value};...;${value}m'
	printf "Values 30..37 are \e[33mforeground colors\e[m\n"
	printf "Values 40..47 are \e[43mbackground colors\e[m\n"
	printf "Value  1 gives a  \e[1mbold-faced look\e[m\n\n"

	# foreground colors
	for fgc in {30..37}; do
		# background colors
		for bgc in {40..47}; do
			fgc=${fgc#37} # white
			bgc=${bgc#40} # black

			vals="${fgc:+$fgc;}${bgc}"
			vals=${vals%%;}

			seq0="${vals:+\e[${vals}m}"
			printf "  %-9s" "${seq0:-(default)}"
			printf " ${seq0}TEXT\e[m"
			printf " \e[${vals:+${vals+$vals;}}1mBOLD\e[m"
		done
		echo; echo
	done
}

[[ -f ~/.extend.bashrc ]] && . ~/.extend.bashrc

[ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion

#get_pwd ()
#{
	#echo $(pwd | sed "s,$HOME,\~,")
#}
#bash_prompt()
#{
	##echo -ne "\001\E[34m\x02\E[1m $(pwd) "
	#if [[ $? == 0 ]]
	#then
		#echo -en " \x02\E[34m\x02\E[1m$(get_pwd)\x02\E[32m\001\E[1m"
	#else
		#echo -en " \x02\E[34m\x02\E[1m$(get_pwd)\x02\E[31m\001\E[1m"
	#fi
	
	#if tty | fgrep pts > /dev/null
	#then
		#echo -en "  "
	#else
		#echo -en " > "
	#fi
#}
#export PS1='$(bash_prompt)\[$(tput sgr0)\]'

#export PS1="\[$(tput bold)\]\[$(tput setaf 1)\][\[$(tput setaf 3)\]\u\[$(tput setaf 2)\]@\[$(tput setaf 4)\]\h \[$(tput setaf 5)\]\W\[$(tput setaf 1)\]]\[$(tput setaf 7)\]\\$ \[$(tput sgr0)\]"
export PS1="\[$(tput sgr0)\]\[$(tput bold)\]\[$(tput setaf 2)\]\u@\h\[$(tput setaf 7)\]:\[$(tput setaf 5)\]\W\\$ \[$(tput sgr0)\]"

HISTSIZE=5000
HISTFILESIZE=5000
shopt -s histappend

source $HOME/.shrc/alias.sh
source $HOME/.shrc/export.sh
