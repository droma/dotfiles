local ret_status=" %(?:%{$fg_bold[green]%} :%{$fg_bold[red]%} %s)"

function get_pwd(){
	pwd | sed "s,$HOME,\~,"
}

PROMPT=' %{$fg_bold[blue]%}$(get_pwd)$(git_prompt_info)%{$reset_color%}
$ret_status%{$reset_color%}'

ZSH_THEME_GIT_PROMPT_PREFIX=" %{$fg[cyan]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY=" %{$fg_bold[yellow]%} %{$reset_color%}"
ZSH_THEME_GIT_PROMPT_CLEAN=" %{$fg_bold[green]%} %{$reset_color%}"

