# Exit if fzf is not installed
if ! builtin type fzf >/dev/null 2>&1; then
  return
fi

# Setup fzf
if [[ ! "$PATH" == */usr/share/fzf/bin* ]]; then
  export PATH="$PATH:/usr/share/fzf/bin"
fi

# Man path
if [[ ! "$MANPATH" == */usr/share/fzf/man* && -d "/usr/share/fzf/man" ]]; then
  export MANPATH="$MANPATH:/usr/share/fzf/man"
fi

# Auto-completion
[[ $- == *i* ]] && source "/usr/share/fzf/shell/completion.zsh" 2> /dev/null

# Key bindings
source "/usr/share/fzf/key-bindings.zsh"
