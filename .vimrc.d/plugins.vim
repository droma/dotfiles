" Devicons on NERDTree & CtrlP
let g:WebDevIconsUnicodeDecorateFolderNodes = 1
let g:webdevicons_enable_airline_statusline = 0
let g:webdevicons_enable_ctrlp              = 1

" NERDTree
map <F1> :NERDTreeToggle<CR>

" NERDCommenter
map <silent> <C-k> <leader>c<space>
let g:NERDUsePlaceHolders = 0

" Airline
set laststatus=2
let g:airline_powerline_fonts = 1
set t_Co=256
let g:airline_theme='jellybeans_v2'

" Enable the list of buffers
let g:airline#extensions#tabline#enabled = 1

" Show just the filename
let g:airline#extensions#tabline#fnamemod         = ':t'
let g:airline#extensions#tabline#buffer_nr_show   = 1
let g:airline#extensions#tabline#buffer_min_count = 2

" Don't show separators
let g:airline_right_alt_sep = ' '
let g:airline_left_alt_sep  = ' '
let g:airline_right_sep = '░'
let g:airline_left_sep  = '▒'
set ruler

" Fix weird right side sections in newer airline
let g:airline_symbols = {}
if filereadable(expand("~/.vim/bundle/vim-airline/plugin/airline.vim"))
	call airline#parts#define_raw('linenr', '%3l')
	call airline#parts#define_accent('linenr', 'bold')
	call airline#parts#define_raw('colnr', ':%2c')
	call airline#parts#define_accent('colnr', 'bold')
	let g:airline_section_z = airline#section#create(['%2p%% ', 'linenr', 'colnr'])
endif

" Change symbols
let g:airline_symbols.spell    = 'S'
let g:airline_symbols.readonly = '[read-only]'

let g:airline_mode_map = {
			\ '__' : '-',
			\ 'n'  : 'N',
			\ 'i'  : 'I',
			\ 'R'  : 'R',
			\ 'c'  : 'C',
			\ 'v'  : 'V',
			\ '' : 'V',
			\ 'V'  : 'V',
			\ 's'  : 'S',
			\ 'S'  : 'S',
		\ }

" Remove unneeded sections
let g:airline_section_y                    = ''
let g:airline_section_error                = {}
let g:airline_section_warning              = {}
let g:airline#extensions#wordcount#enabled = 0

" Better Cpp syntax
let g:cpp_class_scope_highlight     = 1
let g:cpp_member_variable_highlight = 1
let g:cpp_concepts_highlight        = 1

" Clang format
let g:clang_format#style_options = {
	\ 'UseTab'                                    : 'false',
	\ 'IndentWidth'                               : 4,
	\ 'ContinuationIndentWidth'                   : 4,
	\ 'AccessModifierOffset'                      : -4,
	\ 'AlignConsecutiveAssignments'               : 'true',
	\ 'AlignEscapedNewlines'                      : 'Left',
	\ 'PointerAlignment'                          : 'Right',
	\ 'AlignTrailingComments'                     : 'true',
	\ 'AlignConsecutiveDeclarations'              : 'true',
	\ 'BreakBeforeBraces'                         : 'Linux',
	\ 'BinPackParameters'                         : 'true',
	\ 'BinPackArguments'                          : 'true',
	\ 'AllowAllParametersOfDeclarationOnNextLine' : 'true',
	\ }

" Python.
let g:python_highlight_all = 1

" Removes folds in latex sections
let g:Tex_FoldedSections     = ""
let g:Tex_FoldedMisc         = ""
let g:Tex_FoldedEnvironments = "figure,table"
let g:tex_flavor             = "latex"

" Stop latex from overriding F5 binding
let g:Tex_PromptedEnvironments = ""

" Remove latex placeholders
let g:Imap_UsePlaceHolders = 0

" Remove three letter Macros
autocmd Filetype tex,bib call IMAP('ECE', 'ECE', 'tex')
autocmd Filetype tex,bib call IMAP('EEA', 'EEA', 'tex')

" Update ctags each 30 seconds
let g:easytags_update_min    = 30000
" Update tags in subfolders
"let g:easytags_autorecurse = 1
set tags=./.tags,.tags;
let g:easytags_dynamic_files = 1

" CtrlP (in case the mapping is used by something else)
imap <C-P> CtrlP
"let g:ctrlp_working_path_mode = 'c' " Use current directory as search root
let g:ctrlp_cmd='CtrlP :pwd'
let g:ctrlp_use_caching = 0
set wildignore+=*/.git/*,*/build/**,*/venv/**,*/htmlcov/**

" Vim-tmux-navigator
let g:tmux_navigator_no_mappings = 1
nnoremap <silent> <M-H> :TmuxNavigateLeft<CR>
nnoremap <silent> <M-J> :TmuxNavigateDown<CR>
nnoremap <silent> <M-K> :TmuxNavigateUp<CR>
nnoremap <silent> <M-L> :TmuxNavigateRight<CR>

" Snips
imap <C-F> <Esc>:call UltiSnips#JumpForwards()<CR>
nmap <C-F> :call UltiSnips#JumpForwards()<CR>
smap <C-F> <Esc>:call UltiSnips#JumpForwards()<CR>

" Org mode
let g:org_todo_keywords = [['TODO(t)', '|',  'DONE(d)']]
let g:org_agenda_files  = ["~/.org/index.org"]

map <silent> <C-x> :BD<CR>

" If I want to use an autocomplete plugin in the future
set completeopt-=preview

" vimlsp
let g:lsp_diagnostics_echo_cursor = 1
let g:lsp_diagnostics_highlights_insert_mode_enabled = 0

if executable('clangd')
    augroup lsp_clangd
        autocmd User lsp_setup call lsp#register_server({
                    \ 'name': 'clangd',
                    \ 'cmd': {server_info->['clangd']},
                    \ 'whitelist': ['c', 'cpp', 'objc', 'objcpp'],
                    \ })
        autocmd FileType c setlocal omnifunc=lsp#complete
        autocmd FileType cpp setlocal omnifunc=lsp#complete
        autocmd FileType objc setlocal omnifunc=lsp#complete
        autocmd FileType objcpp setlocal omnifunc=lsp#complete
    augroup end
endif

" Install lsp:
" pip install "python-lsp-server[all]" pylsp-mypy python-lsp-black pycodestyle
if executable('pylsp')
    augroup lsp_pylsp
        autocmd User lsp_setup call lsp#register_server({
                    \ 'name': 'pylsp',
                    \ 'cmd': {server_info->['pylsp']},
                    \ 'whitelist': ['python'],
                    \ })
        autocmd FileType python setlocal omnifunc=lsp#complete
    augroup end
endif

nnoremap <leader>dd :LspDefinition<cr>
nnoremap <leader>dn :LspNextDiagnostic<cr>
nnoremap <leader>df :LspReferences<cr>
nnoremap <leader>dr :LspRename<cr>
nnoremap <leader>dp :LspPeekDefinition<cr>
nnoremap <leader>dh :LspHover<cr>

autocmd FileType python nnoremap <leader>dc :DocstringTypes<cr>

" Debugging.
lua <<EOF
local dap = require('dap')
dap.defaults.fallback.terminal_win_cmd = 'below 10split new'

require('dap.ext.vscode').load_launchjs(nil, { lldb = {'c', 'cpp'} })
require('dap-python').setup('python')

-- Setup cppdbg. Install lldb.
local is_win = vim.api.nvim_eval("has('win32')")
if is_win
then
    dap.adapters.lldb = {
      id = 'lldb',
      type = 'executable',
      command = 'C:\\Program Files\\LLVM\\bin\\lldb.exe',
    }
else
    dap.adapters.cppdbg = {
      id = 'cppdbg',
      type = 'executable',
      command = 'OpenDebugAD7.exe'
    }
end
dap.configurations.c = dap.configurations.cpp
dap.configurations.rust = dap.configurations.cpp


-- UI
local dapui = require('dapui')
dapui.setup({
      auto_open = true,
      notify = {
        threshold = vim.log.levels.INFO,
      },
      config = {
        expand_lines = true,
        icons = { expanded = "", collapsed = "", circular = "" },
        mappings = {
          -- Use a table to apply multiple mappings
          expand = { "<CR>", "<2-LeftMouse>" },
          open = "o",
          remove = "d",
          edit = "e",
          repl = "r",
          toggle = "t",
        },
        layouts = {
          {
            elements = {
              { id = "scopes", size = 0.33 },
              { id = "breakpoints", size = 0.17 },
              { id = "stacks", size = 0.25 },
              { id = "watches", size = 0.25 },
            },
            size = 0.33,
            position = "right",
          },
          {
            elements = {
              { id = "repl", size = 0.45 },
              { id = "console", size = 0.55 },
            },
            size = 0.27,
            position = "bottom",
          },
        },
        floating = {
          max_height = 0.9,
          max_width = 0.5, -- Floats will be treated as percentage of your screen.
          border = vim.g.border_chars, -- Border style. Can be 'single', 'double' or 'rounded'
          mappings = {
            close = { "q", "<Esc>" },
          },
        },
      },
    })

EOF

nnoremap <F5> :DapContinue<CR>
nnoremap <S-F5> :DapTerminate<CR>
nnoremap <F9> :DapToggleBreakpoint<CR>
nnoremap <F10> :DapStepOver<CR>
nnoremap <F11> :DapStepInto<CR>
nnoremap <S-F11> :DapStepOut<CR>
