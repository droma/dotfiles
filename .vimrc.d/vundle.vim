" Required for vundle
set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim

if filereadable(expand("~/.vim/bundle/Vundle.vim/autoload/vundle.vim"))
    call vundle#begin()

    Plugin 'VundleVim/Vundle.vim'

    " Aesthetics
    Plugin 'vim-airline/vim-airline'            " Statusbar.

    " Language support
    Plugin 'octol/vim-cpp-enhanced-highlight'   " Better C++ syntax highlighting.
    Plugin 'rhysd/vim-clang-format'             " Auto format C/C++ code.
    Plugin 'tikhomirov/vim-glsl'                " OpenGL Shading Language.
    Plugin 'vim-python/python-syntax'           " Better python syntax highlighting.
    Plugin 'pixelneo/vim-python-docstring'      " Python docstring.

    " LSP
    Plugin 'prabirshrestha/async.vim'
    Plugin 'prabirshrestha/vim-lsp'
    Plugin 'prabirshrestha/asyncomplete.vim'
    Plugin 'prabirshrestha/asyncomplete-lsp.vim'

    " Snippets
    Plugin 'SirVer/ultisnips'                   " Code snippers.
    Plugin 'honza/vim-snippets'                 " More code snippets.

    " MISC
    Plugin 'christoomey/vim-tmux-navigator'     " Switch vim panes with tmux cmds.
    Plugin 'vim-latex/vim-latex'                " Latex support.
    Plugin 'scrooloose/nerdcommenter'           " Comment line(s) with shortcuts.
    Plugin 'scrooloose/nerdtree'                " Sidebar filemanager.
    Plugin 'tpope/vim-eunuch'                   " Unix command support.
    Plugin 'tpope/vim-fugitive'                 " Git support.
    Plugin 'tpope/vim-dispatch'                 " Asynchronous command execution.
    Plugin 'qpkorr/vim-bufkill'                 " Delete buffer without closing window.
    Plugin 'ctrlpvim/ctrlp.vim'                 " Fuzzy search.

    " Orgmode
    Plugin 'jceb/vim-orgmode'                   " Notetaking.
    Plugin 'tpope/vim-speeddating'              " Needed for vim-orgmode.
    Plugin 'mattn/calendar-vim'

    " Debugging
    Plugin 'mfussenegger/nvim-dap'
    Plugin 'mfussenegger/nvim-dap-ui'
    Plugin 'mfussenegger/nvim-dap-python'

    call vundle#end()
endif
