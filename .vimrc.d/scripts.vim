" Remember cursor position when opening file
if has("autocmd")
	au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

function! Edit_other_ext(ext)
	for e in a:ext
		let new_f = "%:p:r." . e
		echo new_f
		if !empty(glob(new_f))
			execute "edit " . new_f
			return
		endif
	endfor
endfunction

" h -> c
" h -> cpp
" h -> cc
" hpp -> cpp
" c -> h
" cc -> h
" cpp -> hpp
function! Switch_header_source()
	if (expand("%:e") == "c")
		call Edit_other_ext(["h"])
	elseif (expand("%:e") == "h")
		call Edit_other_ext(["c", "cpp", "cc", "cxx"])
	elseif (expand("%:e") == "hpp")
		call Edit_other_ext(["cpp"])
	elseif (expand("%:e") == "cpp")
		call Edit_other_ext(["h",  "hpp"])
	elseif (expand("%:e") == "cc")
		call Edit_other_ext(["h", "hpp"])
	endif
endfunction
