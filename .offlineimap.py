# import os, subprocess
from subprocess import check_output

# Maps each mail folder to the pass directory inside the password repo
pass_path = {'IST':     'tecnico.ulisboa.pt',   \
		'Hotmail':      'hotmail.com',          \
		'GMail':        'google.com/mail-app',  \
		'AlbatrozMail': 'albatroz/mail'}

# Retrieves encrypted passwords
def mailpasswd(acct):
	# Using gnupg
	# acct = os.path.basename(acct)
	# path = "/home/droma/.passwd/%s.gpg" % acct
	# args = ["gpg", "--use-agent", "--quiet", "--batch", "-d", path]
	# try:
		# return subprocess.check_output(args).strip()
	# except subprocess.CalledProcessError:
		# return ""

	# Using pass
    return check_output("pass " + pass_path[acct], shell=True).splitlines()[0]
