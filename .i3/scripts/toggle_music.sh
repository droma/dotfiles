#!/bin/sh

track=$(get_track.sh)
if [ -n "$track" ]
then
	echo "Pausing current song"
	# If something's already playing, just toggle
	if pgrep -x cmus ; then
		echo "In CMus"
		cmus-remote -u
	elif pgrep -x ncmpcpp; then
		echo "In NCMPCPP"
		mpc toggle
	else
		echo "In other player"
		playerctl play-pause
	fi
else
	echo "Restarting last song"
	# If nothing is playing, restart last played song
	if pgrep -x cmus ; then
		cmus-remote -r
		cmus-remote -n
		cmus-remote -p
	elif pgrep -x ncmpcpp; then
		mpc prev
		mpc next
		mpc play
	else
		playerctl previous
		playerctl next
		playerctl play
	fi
fi
