#!/bin/sh

if pgrep -x cmus ; then
	cmus-remote -r
elif pgrep -x ncmpcpp ; then
	mpc prev
else
	playerctl previous
fi

