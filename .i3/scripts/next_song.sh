#!/bin/sh

if pgrep -x cmus ; then
	cmus-remote -n
elif pgrep -x ncmpcpp ; then
	mpc next
else
	playerctl next
fi
