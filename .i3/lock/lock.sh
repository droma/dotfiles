#!/bin/bash

lockIcon=$HOME/.i3/lock/small_lock.png
tmpBg=/tmp/screen.png

# Build temporaty image
scrot $tmpBg
convert $tmpBg -scale 10% -scale 1000% $tmpBg
convert $tmpBg $lockIcon -gravity center -composite -matte $tmpBg

# Pauses musicplayer if something is playing
playerStatus=$(playerctl status)
if [ "$playerStatus" = "Playing" ]
then
	playerctl pause
fi

# Lock
i3lock -t -i $tmpBg

# Remove temporary image
rm $tmpBg
