#!/bin/bash

bat_perc () {
	perc=$(acpi 2> /dev/null | cut -d' ' -f4 | cut -d',' -f1)

	if [[ ${#perc} == 0 || "$perc" == "0%" ]]
	then
		#echo "-"
		return
	fi
	
	echo "Bat:$perc"
	return
}

new_mail () {
	mail_count=$($HOME/bin/mail_count)
	if [[ $mail_count > 0 ]]
	then
		echo "Mail"
	fi

	if [[ -f mail/err_file ]]
	then
		echo "Err"
	fi

	return
}

while true
do
	vol_out="Vol:$(awk -F"[][]" '/%/ { print $2 }' <(amixer sget Master) | head -n 1)"
	ip_out="$($HOME/bin/get_ssid)"
	date_out="$(date +"%d %b %Y %H:%M")"

	status_modules=("$vol_out" "$(new_mail)" "$(bat_perc)" "$ip_out" "$date_out")
	out_str=""
	for i in ${!status_modules[*]}
	do
		if [[ ${#status_modules[i]} > 0 ]]
		then
			if [[ $i > 0 ]]
			then
				out_str="$out_str|"
			fi
			out_str="$out_str ${status_modules[i]} "
		fi
	done

	xsetroot -name "$out_str"
	sleep 1
done
