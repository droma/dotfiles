#/bin/bash

# Keyboard settings
setxkbmap -layout pt
setxkbmap -option caps:swapescape
xmodmap -e "keycode 135 = Super_L"
xset r rate 300 30 &
xinput set-prop "ETPS/2 Elantech Touchpad" "libinput Tapping Enabled" 1 &
xinput set-prop 11 305 1 & # For thinkpads only

# Daemons
offlineimap &> /dev/null || touch $HOME/mail/err_file &
redshift &
transmission-daemon &
nm-applet &
systemctl --user $USER docker &
picom &

# Apply NVIDIA GPU settings.
nvidia-settings --load-config-only

# Startup scripts
nitrogen --set-zoom-fill ~/.i3/assets/wallpaper &
betterlockscreen -u ~/.i3/assets/lockpaper &

# DWM Statusbar
~/.dwm/statusbar.sh &

# Workarounds
killall qutebrowser &
