#!/bin/bash

wm="dwm"
action=$(echo -e "logout\npoweroff\nreboot\nsuspend\ncancel" | dmenu "$@" -p "Quit?")

if [[ "$action" == "logout" ]]
then
	kill $(pgrep -x $wm)
fi

if [[ "$action" == "poweroff" ]]
then
	sudo /usr/sbin/shutdown -P now
fi

if [[ "$action" == "reboot" ]]
then
	sudo /usr/sbin/shutdown -r now
fi

if [[ "$action" == "suspend" ]]
then
    systemctl suspend
fi
