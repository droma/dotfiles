#! /bin/bash

if [[ $# -eq 0 ]] ; then
    echo "Provide 1 argument specifying the disto (eg. 'arch')."
    exit 1
fi
install_pkgs="$1"

echo "Installing packages for: $install_pkgs"

basedir=$(dirname "$0")

sudo cp -rf .. "$HOME"

if [[ $install_pkgs == "arch" ]]
then
    "$basedir/pkgs/arch_pkgs.sh"
elif [[ $install_pkgs == "void" ]]
then
    "$basedir/pkgs/void_pkgs.sh"
fi
