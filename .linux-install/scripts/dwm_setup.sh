#! /bin/bash

# DWM
git clone https://gitlab.com/droma/dwm.git
cd dwm
sudo make install
cd ..
rm -rf dwm

# DMenu
git clone https://gitlab.com/droma/dmenu.git
cd dmenu
sudo make install
cd ..
rm -rf dmenu
