#! /bin/bash

# Set zsh with oh-my-zsh
git clone https://github.com/robbyrussell/oh-my-zsh.git "$HOME/.oh-my-zsh"
sudo chsh -s /bin/zsh "$USER"
