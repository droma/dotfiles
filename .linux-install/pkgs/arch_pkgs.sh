#! /bin/bash

# Must have an internet connection to continue
if ping -q -w1 -c1 google.com &> /dev/null
then
	echo "Connected to internet. Starting installation."
else
	echo "No internet connection. Aborting."
	exit 1
fi

# Defines
basedir=$(dirname "$0")
scriptdir="$basedir/../scripts"
pkgsdir="$basedir/../pkgs"
sysdir="$basedir/../sys"

# Basic Installs
sudo pacman -Syyuu --noconfirm
sudo pacman -S --noconfirm $(cat "$pkgsdir/arch_pkgs/pre")

"$scriptdir/zsh_setup.sh"

# Custom pacman and sudoers config
sudo cp -rf $sysdir/pacman.conf /etc/pacman.conf
sudo cp -rf $sysdir/sudoers /etc/sudoers
sudo cp -rf $sysdir/lock_screen /usr/bin
sudo cp -rf $sysdir/suspend@.service /etc/systemd/system
sudo systemctl enable suspend@$USER.service

# Update package databases modified in config file.
sudo pacman -Syyuu --noconfirm

# At this point, passmenu must not be installed.
[[ -e /usr/bin/passmenu ]] && rm /usr/bin/passmenu

# Basic installs
sudo pacman -S --noconfirm $(cat "$pkgsdir/arch_pkgs/basics")
sudo systemctl enable cronie.service

"$scriptdir/yay_setup.sh"

yay -S --noconfirm $(cat "$pkgsdir/arch_pkgs/aur")
yay -S --noconfirm $(cat "$pkgsdir/arch_pkgs/i3")

"$scriptdir/dwm_setup.sh"

# PIP
sudo pip install torrench

# Activate firewall
sudo systemctl enable ufw

# Activate docker.
sudo systemctl enable docker
sudo usermod -aG docker $USER
sudo chmod 666 /var/run/docker.sock

# Teensy support
sudo gpasswd -a $USER dialout
sudo gpasswd -a $USER uucp
wget "https://www.pjrc.com/teensy/49-teensy.rules"
sudo mv ./49-teensy.rules /etc/udev/rules.d/49-teensy.rules

chown -R $USER $HOME

xrdb ../.Xresources
