#!/bin/bash

# Init folders
cd ~
mkdir tmp
mkdir mail

# Custom sudoers and some custom svs config
basedir=$(dirname "$0")
sudo cp -rf $basedir/sudoers /etc/sudoers
sudo cp -rf $basedir/lock_screen /usr/bin

# Basics
sudo xbps-install -Suv
sudo xbps-install -Suv # Not a typo, update at least twice
sudo xbps-install xorg git wget gvim-huge rxvt-unicode urxvt-perls xsel linux-firmware

# DWM
sudo xbps-install base-devel libX11-devel libXft-devel libXinerama-devel libEGL
git clone https://gitlab.com/droma/dwm.git
cd dwm
sudo make install
cd ..
rm -rf dwm

# Dmenu
git clone https://gitlab.com/droma/dmenu.git
cd dmenu
sudo make install
cd ..
rm -rf dmenu

# Network
sudo xbps-install NetworkManager NetworkManager-openvpn network-manager-applet wireless_tools ConsoleKit2 cifs-utils
sudo ln -s /etc/sv/NetworkManager /var/service
sudo ln -s /etc/sv/dbus /var/service
sudo ln -s /etc/sv/consolekit /var/service
sudo ln -s /etc/sv/cgmanager /var/service
sudo xbps-install python-gobject
git clone https://github.com/firecat53/networkmanager-dmenu.git
sudo cp networkmanager-dmenu/networkmanager_dmenu /usr/bin
sudo cp networkmanager-dmenu/networkmanager_dmenu.desktop /usr/share/applications/networkmanager_dmenu.desktop
rm -rf networkmanager-dmenu

# Audio
sudo xbps-install alsa-utils pulseaudio pavucontrol cmus-pulseaudio cmus cmusfm kid3

# Pass
sudo xbps-install pass
git clone https://git.zx2c4.com/password-store
sudo mv password-store/contrib/dmenu/passmenu /usr/bin/
rm -rf password-store

# Term utils
sudo xbps-install neomutt offlineimap msmtp htop python-pip tmux transmission neofetch urlview w3m zsh light libxml2-devel libxslt-devel acpi cronie borg texlive-bin
sudo ln -s /etc/sv/acpid /var/service
sudo ln -s /etc/sv/cronie /var/service
sudo ln -s /etc/sv/crond /var/service
sudo pip install torrench configparser

# Install Vundle and other vim addons
git clone https://github.com/VundleVim/Vundle.vim.git $HOME/.vim/bundle/Vundle.vim
sudo xbps-install ctags
vim +PluginInstall +qall

# Set zsh with oh-my-zsh
git clone https://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh
sudo chsh -s /bin/zsh $USER

# Full font pack
sudo xbps-install -S noto-fonts-emoji noto-fonts-ttf noto-fonts-cjk

# Other apps
sudo xbps-install -S qutebrowser nitrogen lxappearance Thunar engrampa arc-theme mpv gufw redshift kdeconnect zathura feh qpdfview pinetry-gtk
sudo ln -s /etc/sv/ufw /var/service

# Nonfree
sudo xbps-install -Sy void-repo-nonfree
sudo xbps-install -Sy void-repo-multilib
sudo xbps-install -Sy void-repo-multilib-nonfree
sudo xbps-install -Suv
#sudo xbps-install skype steam nvidia-libs-32bit nvidia wine-32bit dropbox

# Remove unnecessary svs
sudo rm /var/service/dhcpcd
sudo rm /var/service/agetty-tty[4-6]
