source ~/.vimrc.d/vundle.vim   " Plugin manager
source ~/.vimrc.d/scripts.vim  " Custom scripts

" Necessary
set nocompatible

" Fonts. Remember to install the specified font.
set encoding=utf8

" Windows specific
if has('win32') || has('win64')
    set runtimepath+=~/.vim
    set guioptions=c " Just code, remove all toolbars.
    set guifont=DejaVu\ Sans\ Mono:h10
else
    set guifont=Input\ Nerd\ Font\ Mono\ Regular\ 11
endif

" Neovim specific.
if has('nvim')
    set title
endif

" Syntax Highlighting & Theming
filetype on
filetype plugin indent on
syntax enable
set grepprg=grep\ -nH\ $*
if !empty(glob("~/.vim/colors/jellybeans_v2.vim"))
    colorscheme jellybeans_v2
    let g:jellybeans_use_gui_italics=0
endif

" Autoindent
set autoindent

set autoread

" Only redraw screen when needed
set lazyredraw

" Print hidden chars
set list
set listchars=tab:>\ ,eol:¬

" Autocomplete
set complete=.,w,b,u,t

" Tabs
set expandtab
set copyindent
set preserveindent
set softtabstop=0
set shiftwidth=4
set tabstop=4

" Fix Alt/Meta keybindings
if !has('nvim')
    set <M-H>=h
    set <M-J>=j
    set <M-K>=k
    set <M-L>=l
endif

" Vim splits
" Resize splits when resizing vim
autocmd VimResized * wincmd =

map <silent> <M-L> :wincmd l<CR>
map <silent> <M-H> :wincmd h<CR>
map <silent> <M-J> :wincmd j<CR>
map <silent> <M-K> :wincmd k<CR>
nmap <silent> <C-W>v :split<CR>
nmap <silent> <C-W>b :vsplit<CR>

" Automatically copy and paste to clipboard
if has('win32') || has('win64')
    set clipboard=unnamed
else
    " Need both
    set clipboard=unnamedplus
    let g:clipbrdDefaultReg = '+'
endif

" Different color after 80 chars. @TODO: This doesn't look good on Windows...
execute "set colorcolumn=".join(range(81,355), ',')

" Folds
"set foldmethod=syntax

" Cool tab completion stuff
set path+=**
set wildmenu
set wildmode=list:longest,full
set wildignorecase

" Enable mouse support in console
set mouse=a

" Shortcuts
"map <Down> gj
"map <Up> gk
map <Up> <nop>
map <Down> <nop>
map <Left> <nop>
map <Right> <nop>
imap <Up> <nop>
imap <Down> <nop>
imap <Left> <nop>
imap <Right> <nop>

" Exit terminal with <Esc>.
tnoremap <Esc> <C-\><C-n>

" When searching, move cursor to middle of screen
map n nzzzv
map N Nzzzv

nnoremap <Tab> :bn!<CR>
nnoremap <S-Tab> :bp!<CR>

" easier moving of code blocks
vnoremap < <gv
vnoremap > >gv

" Got backspace?
set backspace=2

" Line Numbers
set relativenumber
set number

" Casing
set ignorecase
set smartcase

" Search
set incsearch
set hlsearch

" When I close a tab, remove the buffer
set hidden

" Highlight column
set cursorline

" Removes delay from Esc
set timeoutlen=1000 ttimeoutlen=0

" :W will save
command! W :w

let mapleader=','
" Shortcut to switch to header in C/C++
"nnoremap <leader>h :e %:p:s,.h$,.X123X,:s,.cpp$,.h,:s,.X123X$,.cpp,<CR>
nnoremap <leader>h :call Switch_header_source()<CR>
" Open .cpp and respective .h side-by-side
nmap <leader>bc :Bufonly<CR>:vsplit<CR><leader>h
nmap <leader>cb <leader>bc

" Compile macro (using custom script)
map <leader>m :Dispatch! ~/bin/build<CR>

" Automatically remove extra whitespace when leaving insert.
au InsertLeave * :%s/\s\+$//e

" Latex settings
au Filetype tex,bib setlocal synmaxcol=99999
au Filetype tex setlocal spell
au Filetype tex setlocal colorcolumn=
au Filetype tex setlocal wrap
au Filetype tex setlocal linebreak
au Filetype tex noremap <F5> :! make<CR>

" Markdown settings
au Filetype markdown setlocal synmaxcol=99999
au Filetype markdown setlocal expandtab
au Filetype markdown setlocal spell
au Filetype markdown setlocal tabstop=3
au Filetype markdown setlocal colorcolumn=
au Filetype markdown setlocal wrap
au Filetype markdown setlocal linebreak
au Filetype markdown noremap <F5> :! make<CR>

" Org settings
au Filetype org setlocal tabstop=4
au Filetype org setlocal noexpandtab
au Filetype org setlocal noexpandtab
"au BufRead *.org normal zR

" Start with all folds open
au BufRead ~/doc/main.org normal zM
au BufRead ~/doc/main.org normal gg

" Xresources
au BufNewFile,BufRead *.xresources   set syntax=xdefaults

" Mail settings
au BufRead mail setlocal syntax=mail
au BufRead mail setlocal lbr
au Filetype mail setlocal synmaxcol=99999
au Filetype mail setlocal colorcolumn=
au Filetype mail setlocal linebreak
au Filetype mail setlocal wrap
au Filetype mail setlocal spell

if !has('nvim')
    " Use gvimfullscreen.dll: https://github.com/derekmcloughlin/gvimfullscreen_win32/tree/master
    if has('win32') || has('win64')
        map <silent> <F11> :call libcallnr("gvimfullscreen.dll", "ToggleFullScreen", 0)<CR>
        let skip_loading_mswin=1
    endif
else
    " Neovim already has a built-in feature.
    map <silent> <F11> :call rpcnotify(0, 'Gui', 'WindowFullScreen', !expand(g:GuiWindowFullScreen))<CR>
endif

source ~/.vimrc.d/plugins.vim    " Plugin configuration
