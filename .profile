# ~/profile

#Set our umask
umask 022

# Set our default path
PATH="/usr/local/sbin:/usr/local/bin:/usr/bin:$PATH"
export PATH
if [ -f /etc/profile.d/texlive.sh ]
then
	. /etc/profile.d/texlive.sh
fi

# Load profiles from /etc/profile.d
if test -d /etc/profile.d/; then
	for profile in /etc/profile.d/*.sh; do
		test -r "$profile" && . "$profile"
	done
	unset profile
fi

# Source global bash config
if test "$PS1" && test "$BASH" && test -z ${POSIXLY_CORRECT+x} && test -r /etc/bash.bashrc; then
	. /etc/bash.bashrc
fi

# Termcap is outdated, old, and crusty, kill it.
unset TERMCAP

# Man is much better than us at figuring this out
unset MANPATH

export QT_SELECT=5
[ "$XDG_CURRENT_DESKTOP" = "KDE" ] || [ "$XDG_CURRENT_DESKTOP" = "GNOME" ] || export QT_QPA_PLATFORMTHEME="qt5ct"

export MATLAB_JAVA=/usr/lib/jvm/java-8-openjdk/jre

# Start wm if logging from tty1
if [[ $(tty) == /dev/tty1 ]]
then
	#light -S 20
	startx
fi
