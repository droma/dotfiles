# Behavior
c.editor.command              = ["vim", "-f", "{}"]
c.hints.chars                 = 'sadfhjklwerucxmng'
# c.input.insert_mode.auto_load = True
c.input.insert_mode.auto_load = False

# CSS
# c.content.user_stylesheets = "./stylesheets/tomorrow.css"
# c.content.user_stylesheets = "./stylesheets/solarized-all-sites-dark.css"

# Opens pdf in window, instead of downloading. Needs pdfjs to be installed.
# c.content.pdfjs = True
#If backed gives problems, use webengine.
c.backend       = 'webengine'

c.url.start_pages = ["www.google.com"]
c.url.searchengines = {
		"DEFAULT": "https://www.google.com/search?hl=en&q={}",         \
		"w":       "https://en.wikipedia.org/w/index.php?search={}",   \
		"aw":      "https://wiki.archlinux.org/?search={}",            \
		"r":       "https://reddit.com/search?q={}",                   \
		"sr":      "https://reddit.com/r/{}",                          \
		"y":       "https://www.youtube.com/results?search_query={}",  \
		"g":       "https://www.google.com/search?hl=en&q={}",         \
		"t":       "http://www.thesaurus.com/browse/{}?s=t"
}

# Adblock. Must have python-adblock installed.
c.content.blocking.method = 'both'

config.load_autoconfig(False)

# Keybindings
config.bind('<h>',      'back')
config.bind('<l>',      'forward')
config.bind('<t>',      'cmd-set-text -s :open -t')
config.bind('<x>',      'tab-close')
config.bind('<j>',      'scroll-page 0 0.25')
config.bind('<k>',      'scroll-page 0 -0.25')
config.bind('<i>',      'tab-next')
config.bind('<u>',      'tab-prev')
config.bind('<Ctrl-d>', 'download')
config.unbind('<d>',    mode='normal')
config.bind('<z>',      'undo')

# UI

# Downloads
c.downloads.location.directory = '~/tmp'
c.downloads.location.prompt    = False
c.downloads.position           = 'bottom'
c.downloads.remove_finished    = 100000

# Tab bar
c.tabs.favicons.scale = 0.8
c.tabs.padding        = {"top": 1, "bottom": 1, "left": 10, "right": 10}
c.tabs.title.format   = '{current_title}'

color_scheme = {
  'black':          '#121212',
  'bright_black':   '#535551',
  'red':            '#ca674a',
  'bright_red':     '#ea2828',
  'green':          '#96a967',
  'bright_green':   '#87dd32',
  'yellow':         '#d3a94a',
  'bright_yellow':  '#f7e44d',
  'blue':           '#5778c1',
  'bright_blue':    '#6f9bca',
  'magenta':        '#9c35ac',
  'bright_magenta': '#a97ca4',
  'cyan':           '#6eb5f3',
  'bright_cyan':    '#32dddd',
  'white':          '#a9a9a9',
  'bright_white':   '#d7d7d7',
}

# Hints
c.colors.hints.bg = color_scheme['black']
c.colors.hints.fg = color_scheme['bright_white']
c.colors.hints.match.fg = color_scheme['bright_black']
c.hints.border = "1px solid " + color_scheme['black']

# Tabs
c.colors.tabs.bar.bg = color_scheme['black']
c.colors.tabs.indicator.start = color_scheme['blue']
c.colors.tabs.indicator.stop = color_scheme['green']
c.colors.tabs.indicator.error = color_scheme['red']
c.colors.tabs.odd.bg = color_scheme['black']
c.colors.tabs.odd.fg = color_scheme['bright_white']
c.colors.tabs.even.bg = color_scheme['black']
c.colors.tabs.even.fg = color_scheme['bright_white']
c.colors.tabs.selected.even.bg = color_scheme['bright_black']
c.colors.tabs.selected.even.fg = color_scheme['bright_white']
c.colors.tabs.selected.odd.bg = color_scheme['bright_black']
c.colors.tabs.selected.odd.fg = color_scheme['bright_white']

# Statusbar
c.colors.statusbar.insert.fg = color_scheme['black']
c.colors.statusbar.insert.bg = color_scheme['green']
c.colors.statusbar.normal.bg = color_scheme['black']
c.colors.statusbar.normal.fg = color_scheme['bright_white']
c.colors.statusbar.command.bg = color_scheme['black']
c.colors.statusbar.command.fg = color_scheme['bright_white']
c.colors.statusbar.url.fg = color_scheme['bright_white']
c.colors.statusbar.url.success.https.fg = color_scheme['bright_green']
c.colors.statusbar.url.error.fg = color_scheme['bright_red']
c.colors.statusbar.url.hover.fg = color_scheme['cyan']
c.colors.statusbar.progress.bg = color_scheme['bright_white']
c.colors.completion.item.selected.bg = color_scheme['blue']
c.colors.completion.item.selected.border.top = color_scheme['blue']
c.colors.completion.item.selected.border.bottom = color_scheme['blue']

# Downloads
c.colors.downloads.bar.bg = color_scheme['black']
c.colors.downloads.start.fg = color_scheme['black']
c.colors.downloads.start.bg = color_scheme['blue']
c.colors.downloads.stop.fg = color_scheme['black']
c.colors.downloads.stop.bg = color_scheme['green']
c.colors.downloads.error.fg = color_scheme['black']
c.colors.downloads.error.bg = color_scheme['red']

# Dark mode.
c.colors.webpage.darkmode.enabled = True
c.colors.webpage.darkmode.policy.images = 'never'

# Fonts
font = {
	'ui': '10pt monospace',
	'hints': '10pt monospace',
	'prompt': '10pt sans-serif'
}

c.fonts.completion.entry = font['ui']
c.fonts.completion.category = 'bold ' + font['ui']
c.fonts.debug_console = font['ui']
c.fonts.downloads = font['ui']
c.fonts.hints = 'bold ' + font['hints']
c.fonts.keyhint = font['ui']
c.fonts.messages.error = font['ui']
c.fonts.messages.info = font['ui']
c.fonts.messages.warning = font['ui']
c.fonts.prompts = font['prompt']
c.fonts.statusbar = font['ui']
