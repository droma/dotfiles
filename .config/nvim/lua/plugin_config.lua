-- lualine.
require('lualine').setup({
    options = {
        section_separators = '',
        component_separators = ''
    },
    sections = {
        lualine_a = { 'mode' },
        lualine_b = { 'branch', 'diff', 'diagnostics' },
        lualine_c = { 'filename' },
        lualine_x = { 'encoding', 'filetype' },
        lualine_y = { 'progress' },
        lualine_z = { 'location' }
    }
})

-- Barbar
require('bufferline').setup {
    animation = false,
    auto_hide = false,
}
-- Force buffer line to always ordered by the buffer number.
-- This way, you are ensured that when you go to the next/previous buffer, you will go to the buffer
-- that's immediately to the left/right on the bufferline respectively.
vim.api.nvim_create_autocmd({ 'BufAdd', 'BufDelete' }, {
    callback = vim.schedule_wrap(function()
        vim.cmd.BufferOrderByBufferNumber()
    end)
})

-- Treesitter
require 'nvim-treesitter.configs'.setup {
    indent = { enable = true },
    highlight = {
        enable = true,
        -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
        -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
        -- Using this option may slow down your editor, and you may see some duplicate highlights.
        -- Instead of true it can also be a list of languages
        additional_vim_regex_highlighting = false,
    },
    -- logs.
    ensure_installed = {
        'c', 'cpp', 'lua', 'make', 'markdown', 'markdown_inline',
        'matlab', 'python', 'rust', 'vim', 'vimdoc'
    },
    sync_install = false,
}
-- Folding.
vim.wo.foldmethod = 'expr'
vim.wo.foldexpr = 'v:lua.vim.treesitter.foldexpr()'
vim.wo.foldlevel = 99
-- Keymap to toggle fold.
vim.keymap.set('n', '<leader>f', "za<CR>", {})

-- Telescope.
require('telescope').setup({
    defaults = {
        file_ignore_patterns = {
            ".git/", ".git\\",
            "build/", "build\\",
            "build-Linux/", "build-Linux\\",
            "build-Windows/", "build-Windows\\",
            "venv/", "venv\\",
            "htmlcov/", "htmlcov\\",
            "cov_html/", "cov_html\\",
            ".vs/", ".vs\\",
            ".conan/", ".conan\\",
            "target/", "target\\",
            ".mypy_cache/", ".mypy_cache\\",
            "__pycache__/", "__pycache__\\",
            ".pytest_cache/", ".pytest_cache\\",
        }
    },
    pickers = {
        find_files = {
            hidden = true,
            no_ignore = true,
        }
    }
})
local builtin = require('telescope.builtin')
vim.keymap.set('n', '<C-P>', builtin.find_files, {})
vim.keymap.set('n', '<C-F>', builtin.live_grep, {})
-- Workaround to avoid Ctrl-P changing to insert mode.
vim.api.nvim_create_autocmd("WinLeave", {
    callback = function()
        if vim.bo.ft == "TelescopePrompt" and vim.fn.mode() == "i" then
            vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes("<Esc>", true, false, true), "i", false)
        end
    end,
})

-- Comment.
require('Comment').setup({
    toggler = {
        line = '<C-k>',
        block = nil
    },
    opleader = {
        line = '<C-k>',
        block = nil
    },
    mappings = { extra = false }
})

-- Tree.
require('nvim-tree').setup({
    git = {
        enable = false,
    },
    filters = {
        git_ignored = false,
        custom = { ".git", ".cache", "__pycache__" },
    }
})
vim.keymap.set('', '<F1>', ':NvimTreeToggle<CR>', {})

-- Bufkill.
vim.keymap.set('', '<C-x>', ':BD<CR>', { silent = true })

-- Functions for switching between C header and source files.
function file_exists(name)
    local f = io.open(name, "r")
    return f ~= nil and io.close(f)
end

function edit_other_ext(ext)
    for _, e in ipairs(ext) do
        local root = vim.fn.expand("%:r.")
        local other_filename = root .. "." .. e
        local other_exists = file_exists(other_filename)
        if other_exists then
            vim.cmd('e ' .. other_filename)
            return
        end
    end
end

function switch_header_source()
    local extension = vim.fn.expand("%:e")

    local header_source_table = {
        ["h"] = { "c", "cpp", "cc", "cxx" },
        ["c"] = { "h" },
        ["hpp"] = { "cpp" },
        ["cpp"] = { "h", "hpp" },
        ["cc"] = { "h", "hpp" },
    }

    local switched_extension = header_source_table[extension]
    if switched_extension ~= nil then
        edit_other_ext(switched_extension)
    end
end

vim.keymap.set('n', '<leader>h', ':lua switch_header_source()<CR>', { noremap = true })

-- LSP.
vim.lsp.inlay_hint.enable(true)
local on_attach = function(client, bufnr)
    -- Enable completion triggered by <c-x><c-o>
    vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

    -- Mappings.
    local bufopts = { noremap = true, silent = true, buffer = bufnr }
    vim.keymap.set('n', '<leader>dd', vim.lsp.buf.definition, bufopts)
    vim.keymap.set('n', '<leader>dr', vim.lsp.buf.rename, bufopts)
    vim.keymap.set('n', '<leader>df', vim.lsp.buf.references, bufopts)
    vim.keymap.set('n', '<leader>dh', vim.lsp.buf.hover, bufopts)
    vim.keymap.set('n', '<leader>ff', vim.lsp.buf.format, bufopts)
    vim.keymap.set('n', '<leader>dn', vim.diagnostic.goto_next, bufopts)
    vim.keymap.set('n', '<leader>dp', vim.diagnostic.goto_prev, bufopts)
end
local lsp_flags = {
    debounce_text_changes = nil,
}

local coq = require('coq')
require 'lspconfig'.pylsp.setup(coq.lsp_ensure_capabilities({
    on_attach = on_attach,
    flags = lsp_flags,
    settings = {
        pylsp = {
            plugins = {
                pycodestyle = {
                    enabled = true,
                    config = "./.config/.pycodestyle"
                },
                flake8 = {
                    enabled = false,
                    config = "./.config/.flake8"
                },
            }
        }
    }
}))

require 'lspconfig'.clangd.setup(coq.lsp_ensure_capabilities({
    on_attach = on_attach,
    flags = lsp_flags,
}))

require 'lspconfig'.rust_analyzer.setup(coq.lsp_ensure_capabilities({
    on_attach = on_attach,
    flags = lsp_flags,
}))

require 'lspconfig'.lua_ls.setup(coq.lsp_ensure_capabilities({
    on_attach = on_attach,
    flags = lsp_flags,
    settings = {
        Lua = {
            diagnostics = {
                globals = { "vim" },
                disable = { "lowercase-global" },
            }
        }
    }
}))

require 'lspconfig'.cmake.setup(coq.lsp_ensure_capabilities({
    on_attach = on_attach,
    flags = lsp_flags,
}))

require('lsp_signature').setup({
    bind = true,
    always_trigger = true,
    floating_window = true,
    hint_enable = true,
    toggle_key = '<C-s>'
})

-- DAP.
local dap = require('dap')

require('dap.ext.vscode').load_launchjs(nil, { lldb = { 'c', 'cpp', 'rust' } })
require('dap.ext.vscode').json_decode = require'json5'.parse
require('dap-python').setup('python')

dap.adapters.lldb = {
    type = "executable",
    command = (vim.fn.has('win32') == 1 and "lldb-vscode.exe" or "lldb-dap"),
    name = "lldb",
}

local dapui = require('dapui')
dapui.setup()
dap.listeners.after.event_initialized["dapui_config"] = function()
    dapui.open()
end
dap.listeners.before.event_terminated["dapui_config"] = function()
    dapui.close()
end
dap.listeners.before.event_exited["dapui_config"] = function()
    dapui.close()
end

vim.keymap.set('n', '<F4>', ":lua require'dap'.run_last()<CR>", { noremap = true, silent = true })
vim.keymap.set('n', '<F5>', ":DapContinue<CR>", { noremap = true, silent = true })
vim.keymap.set('n', '<S-F5>', ":DapTerminate<CR>", { noremap = true, silent = true })
vim.keymap.set('n', '<F9>', ":DapToggleBreakpoint<CR>", { noremap = true, silent = true })
vim.keymap.set('n', '<F10>', ":DapStepOver<CR>", { noremap = true, silent = true })
vim.keymap.set('n', '<F11>', ":DapStepInto<CR>", { noremap = true, silent = true })
vim.keymap.set('n', '<S-F11>', ":DapStepOut<CR>", { noremap = true, silent = true })
-- Sometimes <S-FN> combinations dont work but emit a different <FM> key.
-- Shift + F5.
vim.keymap.set('n', '<F15>', ":DapTerminate<CR>", { noremap = true, silent = true })
-- Shift + F11.
vim.keymap.set('n', '<F23>', ":DapStepOut<CR>", { noremap = true, silent = true })

-- Neogen.
require('neogen').setup {
    enabled = true,
    input_after_comment = true,

    -- Specify annotation template.
    languages = {
        python = {
            template = {
                annotation_convention = "reST"
            }
        }
    }
}
local opts = { noremap = true, silent = true }
vim.keymap.set('n', '<leader>gdf', ":lua require('neogen').generate()<CR>", opts)
vim.keymap.set('n', '<leader>gdc', ":lua require('neogen').generate({type='class'})<CR>", opts)

-- Navigation inside tmux.
require('nvim-tmux-navigation').setup {
    keybindings = {
        left = "<M-h>",
        right = "<M-l>",
        up = "<M-k>",
        down = "<M-j>",
    },
    automatic_installation = true,
}

-- Mason.
require('mason').setup {}
require('mason-lspconfig').setup {
    ensure_installed = {
        "clangd",
        "cmake",
        "lua_ls",
        "pylsp",
        "rust_analyzer",
    }
}
require('mason-nvim-dap').setup {
    ensure_installed = {
        "codelldb",
        "debugpy",
    },
    automatic_installation = true,
}

require('gitsigns').setup {
    current_line_blame = true,
}
-- Set better colorscheme on hunks.
vim.api.nvim_set_hl(0, "DiffDelete", {
    link = "@diff.minus",
})
vim.api.nvim_set_hl(0, "DiffAdd", {
    link = "@diff.plus",
})
vim.keymap.set("n", "<leader>gd", "<CMD>Gitsigns preview_hunk<CR>")

require('diffview').setup {}
vim.keymap.set("n", "<leader>gv", "<CMD>DiffviewOpen<CR>")
vim.keymap.set("n", "<leader>_g", "<CMD>DiffviewClose<CR>")
vim.keymap.set("n", "<leader>gh", "<CMD>DiffviewFileHistory<CR>")

require("oil").setup {
    delete_to_trash = true,
    view_options = {
        show_hidden = true,
    }
}
vim.keymap.set("n", "-", "<CMD>Oil<CR>", { desc = "Open parent directory" })

require("outline").setup {
    width = 25,
    relative_width = true,
    auto_close = true,
    auto_jump = true,
    focus_on_open = true,
    keymaps = {
        -- Always close pane when jumping to symbol.
        -- The `auto_close` setting doesn't seem to work, so this is a workaround.
        goto_location = {},
        goto_and_close = '<Cr>'
    },
    symbols = {
        filter = {
            default = { 'String', exclude=true },
            python = { 'Function', 'Class' },
        }
    }
}
vim.keymap.set("n", "<leader>o", "<CMD>Outline<CR>")

overseer_constants = require('overseer.constants')
require('overseer').setup {
    component_aliases = {
        default = {
            { "open_output",        on_start = "always" },
            { "on_exit_set_status", success_codes = { 0 } },
        },
    },
}
-- Function for rerunning last task.
vim.api.nvim_create_user_command("OverseerRerun", function()
    local overseer = require("overseer")
    local tasks = overseer.list_tasks({ recent_first = true })
    if vim.tbl_isempty(tasks) then
        -- Task not found, list the tasks.
        overseer.run_template()
    else
        overseer.run_action(tasks[1], "restart")
    end
end, {})
-- (Re)run task.
vim.keymap.set("n", "<leader>b", "<CMD>OverseerRerun<CR>")
-- Run new task.
vim.keymap.set("n", "<leader>B", "<CMD>OverseerRun<CR>")
-- Hide window.
vim.keymap.set("n", "<leader>_b", "<CMD>lua require('overseer').close()<CR>")

require('dressing').setup()
require('alpha').setup(require 'alpha.themes.startify'.config)
