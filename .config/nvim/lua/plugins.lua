-- Install package manager.
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)

-- This needs to be set before, otherwise it wont work.
vim.g.coq_settings = { auto_start = "shut-up" }


local plugins = {
    -- Colorscheme.
    { 'rktjmp/lush.nvim' },
    { 'metalelf0/jellybeans-nvim' },

    -- Status line.
    {
        'nvim-lualine/lualine.nvim',
    },

    -- Tab bar
    {
        'romgrk/barbar.nvim',
        dependencies = {
            'lewis6991/gitsigns.nvim',
            'nvim-tree/nvim-web-devicons',
        }
    },

    -- Comments.
    {
        'numToStr/Comment.nvim',
    },

    -- Side-bar.
    {
        'nvim-tree/nvim-tree.lua',
        dependencies = {
            'nvim-tree/nvim-web-devicons',
        },
    },

    -- Close buffers.
    { 'qpkorr/vim-bufkill' },

    -- Git integration.
    { 'tpope/vim-fugitive' },
    { 'lewis6991/gitsigns.nvim' },
    { 'sindrets/diffview.nvim' },

    -- LSP
    {
        'neovim/nvim-lspconfig',
        lazy = false,
    },
    { 'ms-jpq/coq_nvim',         branch = 'coq' },
    { 'ms-jpq/coq.artifacts',    branch = 'artifacts' },
    { 'ms-jpq/coq.thirdparty',   branch = '3p' },
    { 'ray-x/lsp_signature.nvim' },

    -- Debugging
    {
        'mfussenegger/nvim-dap',
        dependencies = {
            'nvim-neotest/nvim-nio',
        },
    },
    { 'rcarriga/nvim-dap-ui',        version = "v3.9.3" },
    { 'mfussenegger/nvim-dap-python' },

    -- Code generation.
    {
        "danymat/neogen",
        dependencies = "nvim-treesitter/nvim-treesitter",
        -- Ensure latest release is used (for Windows support).
        version = false
    },

    -- Testing.
    {
        "nvim-neotest/neotest",
        dependencies = {
            "nvim-neotest/nvim-nio",
            "nvim-lua/plenary.nvim",
            "antoinemadec/FixCursorHold.nvim",
            "nvim-treesitter/nvim-treesitter",
            "alfaix/neotest-gtest",
            "rouge8/neotest-rust",
            "nvim-neotest/neotest-python",
        },
    },

    -- Misc.
    {
        'nvim-treesitter/nvim-treesitter',
        build = ":TSUpdate",
    },
    {
        'nvim-telescope/telescope.nvim',
        dependencies = { 'nvim-lua/plenary.nvim' }
    },
    { 'stevearc/dressing.nvim' },
    {
        'goolord/alpha-nvim',
        dependencies = { 'nvim-tree/nvim-web-devicons' },
    },
    { 'tpope/vim-surround' },
    { 'alexghergh/nvim-tmux-navigation' },
    { 'williamboman/mason.nvim' },
    { 'williamboman/mason-lspconfig.nvim' },
    { 'jay-babu/mason-nvim-dap.nvim' },
    { 'stevearc/oil.nvim' },
    { 'hedyhli/outline.nvim' },
    {
        'stevearc/overseer.nvim',
        opts = {},
    },
}
-- Add plugins with OS specific build commands.
if vim.fn.has('win32') then
    table.insert(plugins, { 'Joakker/lua-json5', build = '.\\install.ps1'})
else
    table.insert(plugins, { 'Joakker/lua-json5', build = './install.sh'})
end

local opts = {}

require("lazy").setup(plugins, opts)
