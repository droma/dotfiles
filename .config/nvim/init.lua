-- Load plugins.
require('plugins')

-- Syntax highlighting & theming.
vim.opt.termguicolors = true
vim.cmd[[colorscheme jellybeans-nvim]]
-- Floating windows colorscheme.
vim.api.nvim_set_hl(0, "NormalFloat", {
  link = "Normal",
})
vim.api.nvim_set_hl(0, "FloatBorder", {
  bg = "bg",
  fg = "#dddddd", -- whatever your border color for the float is, check it with :highlight
})

-- Misc.
vim.opt.title = true
vim.opt.autoindent = true
vim.opt.autoread = true
vim.opt.lazyredraw = true

-- Line Numbers.
vim.opt.relativenumber = true
vim.opt.number = true

-- Casing.
vim.opt.ignorecase = true
vim.opt.smartcase = true

-- Search.
vim.opt.incsearch = true
vim.opt.hlsearch = true

-- Highlight column.
vim.opt.cursorline = true

-- Print hidden chars.
vim.opt.listchars = { tab = '> ', eol = '¬' }
vim.opt.list = true

-- Tabs.
vim.opt.expandtab = true
vim.opt.copyindent = true
vim.opt.preserveindent = true
vim.opt.softtabstop = 0
vim.opt.shiftwidth = 4
vim.opt.tabstop = 4

-- Resize splits when resizing vim window.
vim.api.nvim_create_autocmd("VimResized", {
    command = "wincmd ="
})
vim.opt.splitbelow = true
vim.opt.splitright = true

-- Clipboard.
vim.opt.clipboard='unnamedplus'

-- Enable mouse support.
vim.opt.mouse='a'

-- Remove delay from Esc.
vim.opt.timeoutlen = 1000
vim.opt.ttimeoutlen = 0

-- Misc shortcuts.
-- Disable arrow keys.
vim.keymap.set({'i', ''}, '<Up>', '<nop>', {})
vim.keymap.set({'i', ''}, '<Down>', '<nop>', {})
vim.keymap.set({'i', ''}, '<Left>', '<nop>', {})
vim.keymap.set({'i', ''}, '<Right>', '<nop>', {})
-- Split control.
vim.keymap.set('n', '<C-W>v', ':split<CR>', { silent = true })
vim.keymap.set('n', '<C-W>b', ':vsplit<CR>', { silent = true })
-- Exit terminal with <Esc>.
vim.keymap.set('t', '<Esc>', '<C-\\><C-n>', {})
-- When searching, move cursor to middle of screen.
vim.keymap.set('', 'n', 'nzzzv', {})
vim.keymap.set('', 'N', 'Nzzzv', {})
-- Change tabs with the <Tab> key.
vim.keymap.set('', '<Tab>', ':bn!<CR>', {})
vim.keymap.set('', '<S-Tab>', ':bp!<CR>', {})
-- Move code blocks.
vim.keymap.set('v', '<', '<gv', { noremap = true })
vim.keymap.set('v', '>', '>gv', { noremap = true })

vim.g.mapleader = ','

-- Automatically remove extra whitespace when leaving insert.
vim.api.nvim_create_autocmd("InsertLeave", {
    callback = function()
        -- Save position so we can restore it after.
        pos = vim.fn.getpos('.')
        vim.cmd(":%s/\\s\\+$//e")
        vim.fn.setpos('.', pos)
    end,
})
-- Reload file if changed externally.
vim.opt.updatetime = 500
vim.api.nvim_create_autocmd(
    {"FocusGained", "FocusLost", "CursorHold"},
    {
        command = ":checktime"
    })

-- Neovide configuration.
if vim.g.neovide then
    vim.g.neovide_cursor_trail_size = 0
    vim.opt.termguicolors = true
end

require('plugin_config')
